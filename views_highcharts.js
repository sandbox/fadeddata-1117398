Drupal.behaviors.views_highcharts = function (context) {
  Drupal.settings.views_highcharts_charts = {};
  
  $.each(Drupal.settings.views_highcharts, function(idx, chart) {
    Drupal.settings.views_highcharts_charts[idx] = new Highcharts.Chart(chart);
  });
}