<?php
/**
 * Implementation of views_plugin_style
 <em>
*/

class views_plugin_style_highcharts extends views_plugin_style {
  function option_definition() {
    $options = parent::option_definition();
    $options['credits'] = array('default' => 0);
    $options['plot'] = array('default' => '');
    $options['pie']['title'] = array('default' => '');

    return $options;
  }
  
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    
    $form['credits'] = array(
      '#type' => 'select', 
      '#title' => t('Credits'), 
      '#default_value' => $this->options['credits'],
      '#options' => array(
        'Disabled',
        'Enabled'
      ),
    );

    $form['plot'] = array(
      '#type' => 'select',
      '#title' => t('Plot Type'),
      '#options' => array(
        'pie' => 'Pie',
        'bar' => 'Bar'
      ),
      '#default_value' => $this->options['plot'],
    );

    // We wrap our fieldsets in a div so we can use dependent.js to
    // show/hide our fieldsets.
    $form['pie-prefix'] = array(
      '#type' => 'hidden',
      '#id' => 'pie-options-wrapper',
      '#prefix' => '<div><div id="pie-options-wrapper">',
      '#process' => array('views_process_dependency'),
      '#dependency' => array('edit-style-options-plot' => array('pie')),
    );

    $form['pie'] = array(
      '#type' => 'fieldset',
      '#title' => t('Pie Plot options'),
      '#collapsible' => TRUE,
      '#attributes' => array('class' => 'pie'),
    );

    $form['pie']['title'] = array(
      '#type' => 'textfield', 
      '#title' => t('Title'), 
      '#default_value' => $this->options['pie']['title'], 
    );

    $form['pie-suffix']  = array(
      '#value' => '</div></div>',
    );
  }
}