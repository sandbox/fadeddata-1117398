<?php

/**
* Implementation of hook_views_plugins
*/
function views_highcharts_views_plugins() {
  $plugins['style']['highcharts'] = array(
    'title' => t('Highcharts'),
    'theme' => 'views_highcharts',
    'help' => t('Render content as a chart..'),
    'handler' => 'views_plugin_style_highcharts',
    'uses row plugin' => FALSE,
    'uses fields' => TRUE,
    'uses options' => TRUE,
    'type' => 'normal',       
  );

  return $plugins;
}
